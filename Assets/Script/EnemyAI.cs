﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Seeker))]

public class EnemyAI : MonoBehaviour
{

    //What to chase?
    public Transform target;

    //How many time AI's path will be change in each second
    public float updateRate = 2f;

    //Caching
    private Seeker seeker;
    private Rigidbody2D rb;

    //The calculated path
    public Path path;

    //The AI's speed per second
    public float speed = 300f;
    public ForceMode2D fMode;

    [HideInInspector]
    public bool pathIsEnded = false;

    //The max distance from the AI to a waypoint to continue to the next waypoint
    public float nextWaypoinDistance = 3f;

    //The waypint we are currently moving toward
    private int currentWayPoint = 0;
    public bool searchinForPlayer = false;
    private void Start()
    {
        seeker = GetComponent<Seeker>();
        rb = GetComponent<Rigidbody2D>();
        if (target == null)
        {
            if (!searchinForPlayer)
            {
                searchinForPlayer = true;
                StartCoroutine(SearchForPlayer());
            }
            return;
        }

        //Start new path to target position , return the result to the OnPathComplere method
        seeker.StartPath(transform.position, target.position, OnPathCompler);

        StartCoroutine(UpdatePath());
    }


    IEnumerator SearchForPlayer()
    {
        GameObject sResult = GameObject.FindGameObjectWithTag("Player");
        if(sResult  == null)
        {
            yield return new WaitForSeconds(0.5f);
            StartCoroutine(SearchForPlayer());
        }
        else
        {
            target = sResult.transform;
            searchinForPlayer = false;
            StartCoroutine(UpdatePath());
            yield return null;
        }
        
    }


    private IEnumerator UpdatePath()
    {
        if (target == null)
        {
            if (!searchinForPlayer)
            {
                searchinForPlayer = true;
                StartCoroutine(SearchForPlayer());
            }
            yield return null;
        }
      
        //Start new path to target position , return the result to the OnPathComplere method
       if(target !=null)
            seeker.StartPath(transform.position, target.position, OnPathCompler);  //**

        yield return new WaitForSeconds(1f / updateRate);

        StartCoroutine(UpdatePath());
    }

    public void OnPathCompler(Path p)
    {
       // Debug.Log("We got the path did we have error" + p.error);
        if (!p.error)
        {
            path = p;
            currentWayPoint = 0;
        }
    }

    private void FixedUpdate()
    {
        if (path == null)
            return;


        if (currentWayPoint >= path.vectorPath.Count)
        {
            if (pathIsEnded)
                return;
            //Debug.Log("End of Path is reached .");
            pathIsEnded = true;
            return;
        }

       
        pathIsEnded = false;

        //Direction to next waypoint
        Vector3 dir = (path.vectorPath[currentWayPoint] - transform.position).normalized;
        dir *= speed * Time.fixedDeltaTime;

        //Move the AI
        rb.AddForce(dir, fMode);

        float dist = Vector3.Distance(transform.position, path.vectorPath[currentWayPoint]);

        if (dist < nextWaypoinDistance)
        {
            currentWayPoint++;
            return;
        }

    }

}
