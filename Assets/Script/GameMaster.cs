﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour
{

    public static GameMaster gm;
    CameraShake cm;
    private void Awake()
    {

        if (gm == null)
        {
            gm = this;
        }
    }

    public Transform playerPrefab;
    public Transform spawnPoint;
    public float spawnDelay = 2;
    public Transform spawnPrefab;

    public CameraShake cameraShake;

    public GameObject gameOverUI;

    //cache
    private AudioManager audioManager;

    public string playerRespawnCountdownSoundName = "RespawnCountdownSound";
    public string gameOverSoundName = "GameOver";
    public string spawnSoundName = "Spawn";

    [SerializeField]
    private int maxLives = 3; 
    private static int _remainingLives;
    public static int RemainingLives
    {
        get { return _remainingLives; }
    }

  
    private void Start()
    {
        if (cameraShake == null)
            Debug.LogError("No camera shake reference in GameMaster");

        _remainingLives = maxLives;

        //caching
        audioManager = AudioManager.instance;
        if (audioManager == null)
            Debug.LogError("No AudioManager instance found !");
    }

    public IEnumerator RespawnPlayer()
    {
        audioManager.PlaySound(playerRespawnCountdownSoundName);
        yield return new WaitForSeconds(spawnDelay - 0.2f);
        Transform clone = Instantiate(spawnPrefab, spawnPoint.position, spawnPoint.rotation) as Transform;

        
        yield return new WaitForSeconds(spawnDelay - ((spawnDelay - 1f) + 0.8f));
        audioManager.PlaySound(spawnSoundName);
        Instantiate(playerPrefab, spawnPoint.position, spawnPoint.rotation);

        Destroy(clone.gameObject, 3f);

    }

    public void GameOver()
    {
        audioManager.PlaySound(gameOverSoundName);
        Debug.Log("Game Over");
        gameOverUI.SetActive(true);
    }

    public static void KillPlayer(Player player)
    {
        Destroy(player.gameObject);
        _remainingLives -= 1;
        if (_remainingLives <= 0)
        {
            gm.GameOver();
        }
        else
        {
            
            gm.StartCoroutine(gm.RespawnPlayer());
            
        }
    }

    public static void KillEnemy(Enemy enemy)
    {
        gm._KillEnemy(enemy);
    }

    public void _KillEnemy(Enemy _enemy)
    {
        //Playe enemy death sound
        audioManager.PlaySound(_enemy.deathSoundName);

        //Instantiating particle system
        Transform _clone = Instantiate(_enemy.deathParticles, _enemy.transform.position, _enemy.transform.rotation) as Transform;
        Destroy(_clone.gameObject, 2f);
        
        //shake camera
        cameraShake.Shake(_enemy.shakeAmt, _enemy.shakeLength);
        Destroy(_enemy.gameObject);
    }
}