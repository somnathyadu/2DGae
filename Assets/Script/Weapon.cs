﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

    public float fireRate = 0;
    public int damage = 10;
    public LayerMask whatToHit;
    float timeToFire = 0;

    public Transform BulletTrailPrefab;
    Transform firePoint;
    public Transform MuzzleFlashPrefab;
    public Transform HitPrefab;

    //To control CameraShake
    public float shakeAmt = 0.05f;
    public float shakeLength = 0.08f;
    CameraShake camShake;

    [SerializeField]
    string pistoleSound = "PistoleSound";

    //caching
    AudioManager audioManager;

    float timeToSpawnEffect = 0f;
    public float effectSpwanRate = 10;

    void Awake () {
        firePoint = transform.Find("FirePoint");
        if(firePoint == null)
        {
            Debug.LogError("Not firePoint in the Gun");
        }
	}

    private void Start()
    {
        camShake = CameraShake.camShake.GetComponent<CameraShake>();
        if(camShake == null)
        {
            Debug.LogError("No CameraShake script found");
        }

        audioManager = AudioManager.instance;
        if (audioManager == null)
            Debug.LogError("No reference to Audio Manager Script");
    }

    // Update is called once per frame
    void Update () {
    
        if(fireRate == 0)
        {
            if(Input.GetButtonDown("Fire1"))
            {
                Shoot();
            }
        }
        else
        {
            if(Input.GetButton ("Fire1") && Time.time > timeToFire)
            {
                timeToFire = Time.time + 1 / fireRate;
                Shoot();
            }
        }

	}

    void Shoot()
    {
        //To get the position of mouse
        Vector2 mousePosition = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
        Vector2 firePointPosition = new Vector2(firePoint.position.x, firePoint.position.y);
        RaycastHit2D hit = Physics2D.Raycast(firePointPosition, (mousePosition - firePointPosition) * 100, 100, whatToHit);
      
        Debug.DrawLine(firePointPosition, mousePosition);
        if(hit.collider != null)
        {
            Debug.DrawLine(firePointPosition,hit.point , Color.red);
           
            Enemy enemy = hit.collider.GetComponent<Enemy>();
            if(enemy != null)
            {
                enemy.DamageEnemy(damage);
              //  Debug.Log("We hit " + hit.collider.name + "and did" + damage + "damage.");
            }
        }

        if (Time.time > timeToSpawnEffect)
        {
            Vector3 hitPos;
            Vector3 hitNormal;

            if (hit.collider == null)
            {
                hitPos = (mousePosition - firePointPosition) * 60;
                hitNormal = new Vector3(9999, 9999, 9999);
            }
            else
            { 
                hitPos = hit.point;
                hitNormal = hit.normal;
            }
            Effect(hitPos, hitNormal);
            timeToSpawnEffect = Time.time + 1 / effectSpwanRate;
        }
    }
    void Effect(Vector3 hitPos , Vector3 hitNormal)
    {
        Transform trail = Instantiate(BulletTrailPrefab, firePoint.position, firePoint.rotation ) as Transform;
        LineRenderer lr = trail.GetComponent<LineRenderer>();
        if(lr != null)
        {
            lr.SetPosition(1, firePoint.position);
            lr.SetPosition(0, hitPos);
        }

        Destroy(trail.gameObject, 0.04f);

        if(hitNormal != new Vector3(9999, 9999, 9999))
        {
            Transform hitParticle =  Instantiate(HitPrefab, hitPos, Quaternion.FromToRotation(Vector3.right, hitNormal));
            Destroy(hitParticle.gameObject, 1f);
        }

        Transform clone = Instantiate(MuzzleFlashPrefab, firePoint.position, firePoint.rotation) as Transform;
        clone.parent = firePoint;
        float size = Random.Range(0.6f, 0.9f);
        clone.localScale = new Vector3(size, size, 0);
        Destroy(clone.gameObject, 0.02f);

        //Shake camera
        camShake.Shake(shakeAmt, shakeLength);

        //Play shot sound
        audioManager.PlaySound(pistoleSound);
    }
}

