﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    [System.Serializable]
    public class PlayerStats
    {
        public int maxHealth = 120;
        private int _curHealth;
        public int curHealth
        {
            get { return _curHealth; }
            set { _curHealth = Mathf.Clamp(value, 0, maxHealth); }
        }

        public PlayerStats()
        {
            curHealth = maxHealth;
        }
    }
    public PlayerStats stats = new PlayerStats();

    AudioManager audioManager;
    public string deathSoundName = "PlayerDeathSound";
    public string playerGruntSound_1_Name = "PlayerGrunt1";
    public string playerGruntSound_2_Name = "PlayerGrunt2";

    [SerializeField]
    private StatusIndicator statusIndicator;

    private void Start()
    {
        if (statusIndicator == null)
            Debug.LogError("No status indicator reference on player");
        else
            statusIndicator.SetHealth(stats.curHealth, stats.maxHealth);
        audioManager = AudioManager.instance;
        if (audioManager == null)
            Debug.LogError("No instance found of Audio Manager script");

    }

    private void Update()
    {
        if(transform.position.y <= -20)
        {
            DamagePlayer(999);
        }
    }


    public void DamagePlayer(int damage)
    {
        stats.curHealth -= damage;
        if(stats.curHealth <= 0)
        {
            audioManager.PlaySound(deathSoundName);
            GameMaster.KillPlayer(this);
        }
        else
        {
            int r = Random.Range(0, 1);
            if (r == 0)
                audioManager.PlaySound(playerGruntSound_1_Name);
            else
                audioManager.PlaySound(playerGruntSound_2_Name);
        }
        statusIndicator.SetHealth(stats.curHealth, stats.maxHealth);
    }
	
}
