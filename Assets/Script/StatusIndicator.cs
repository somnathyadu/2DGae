﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusIndicator : MonoBehaviour {
    [SerializeField]
    private RectTransform healthBar;
    [SerializeField]
    private Text healthText;

    private void Start()
    {
        if (healthBar == null)
            Debug.LogError("Status Indicator: No health bar object found");
        if (healthText == null)
            Debug.LogError("Status Indicator: No heath text object found");
    }

    public void SetHealth(int _cur, int _max)
    {
        float _value = (float)_cur / _max;

        healthBar.localScale = new Vector3(_value, healthBar.localScale.y, healthBar.localScale.z);
        healthText.text= _cur + "/" + _max + " HP";

    }

}
