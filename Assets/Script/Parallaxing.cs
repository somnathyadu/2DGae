﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallaxing : MonoBehaviour {

    public Transform[] background;   // Araay of back- and foreground objects to be parallax
    private float[] parallaxScale;            // The proportion ofcamera's moement to move the background  by
    public float smoothing =  1f;                  //How smooth the parallax is going to be. Make sure to set this above 0

    private Transform cam;
    private Vector3 PreviousCamPos;

    //Is called before start(). Great for references
    private void Awake()
    {
        //set up camera references 
        cam = Camera.main.transform;

    }
    // Use this for initialization
    void Start () {
        //The previous frame had the current frame's camera position
        PreviousCamPos = cam.position;

        parallaxScale = new float [background.Length];

        for(int i=0; i<background.Length; i++)
        {
            parallaxScale[i] = background[i].position.z*-1;
        }
	}
	
	// Update is called once per frame
	void Update () {
        //for each background 
        for (int i = 0; i < background.Length; i++)
        {
            //The parallax is opposite of the camera's movement bacause the previous frame is multiplied by one
            float parallax = (PreviousCamPos.x - cam.position.x) * parallaxScale[i];

            //set the target position which is current position plus the parallax
            float backgroundTargetPosX = background[i].position.x + parallax;

            //create a target position which is backgrounds current position with it's its target x position
            Vector3 backgroundTargetPos = new Vector3(backgroundTargetPosX, background[i].position.y, background[i].position.z);

            //fade between the position and the target position using lerp
            background[i].position = Vector3.Lerp(background[i].position, backgroundTargetPos, smoothing * Time.deltaTime);
        }
        //Set previous cams position to the position of camera at the end of freme
        PreviousCamPos = cam.position;
	}
}
