﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))] //Unity will cheack if component attached if not it will create one

public class Tiling : MonoBehaviour
{

    public int offsetX = 2;

    //To check if we have to instantiate objects 
    public bool hasARightBuddy = false;
    public bool hasALeftBuddy = false;

    public bool reverseScale = false; //used if object is not tilable

    public float spriteWidth = 0f; //the width of our element
    private Camera cam;
    private Transform myTransform;
    private void Awake()
    {
        cam = Camera.main;
        myTransform = transform;
    }
    void Start()
    {
        SpriteRenderer sRenderer = GetComponent<SpriteRenderer>();
        spriteWidth = sRenderer.sprite.bounds.size.x;
    }

    void Update()
    {
        //does it need buddies? If not do nothing
        if (hasARightBuddy == false || hasALeftBuddy == false)
        {
            //It will calculate the position of camera from center of the player
            float camHorizontalExtend = cam.orthographicSize * Screen.width / Screen.height;

            //calculate the x position where the camera can see the edge of the sprite(element)
            float edgeVisiblePositinOnRight = (myTransform.position.x + spriteWidth / 2) - camHorizontalExtend;
            float edgeVisiblePositionOnLeft = (myTransform.position.x - spriteWidth / 2) + camHorizontalExtend;

            //Checking if we need new buddy
            if (cam.transform.position.x >= edgeVisiblePositinOnRight - offsetX && hasARightBuddy == false)
            {
                MakeNewBuddy(1);
                hasARightBuddy = true;
            }

            else if (cam.transform.position.x <= edgeVisiblePositionOnLeft + offsetX && hasALeftBuddy == false)
            {
                MakeNewBuddy(-1);
                hasALeftBuddy = true;

            }
        }
    }

    void MakeNewBuddy(int LeftOrRight)
    {
        //calculating the position of our new buddy
        Vector3 newPosition = new Vector3(myTransform.position.x + spriteWidth * LeftOrRight, myTransform.position.y, myTransform.position.z);
        Transform newBuddy = Instantiate(myTransform, newPosition, myTransform.rotation) as Transform;

        if (reverseScale == true)
        {
            newBuddy.localScale = new Vector3(newBuddy.localScale.x * -1, newBuddy.localScale.y, newBuddy.localScale.z);
        }
        newBuddy.parent = myTransform.parent;

        if (LeftOrRight > 0)
        {
            newBuddy.GetComponent<Tiling>().hasALeftBuddy = true;
        }
        else
        {
            newBuddy.GetComponent<Tiling>().hasARightBuddy = true;
        }
    }
}