﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmRotation : MonoBehaviour {
    public int rotationOffset = 90;
	

	void Update () {
        //subtracting the position of mouse from player position
        Vector3 distance = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
       distance.Normalize();   // normalize the vector . Meaning that all the sum of the vector will be equal to 1.\
                    //Finding the angle between mouse and horizontal and converting it from radian to degree.
        float rotZ = Mathf.Atan2(distance.y, distance.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rotZ + rotationOffset);

	}
}
