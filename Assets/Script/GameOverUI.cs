﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class GameOverUI : MonoBehaviour {

    AudioManager audioManager;

    private void Start()
    {
        audioManager = AudioManager.instance;
        if (audioManager == null)
            Debug.Log("No instance of Audio Manager found ");
    }

    public void Quit()
    {
        audioManager.PlaySound("ButtonePress");
        Debug.Log("Application Quit!");
        Application.Quit();
    }

    public void Retry()
    {
        audioManager.PlaySound("ButtonePress");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void OnMouseHover()
    {
        audioManager.PlaySound("HoverSound");
    }
}
