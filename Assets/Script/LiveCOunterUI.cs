﻿using UnityEngine.UI;
using UnityEngine;

[RequireComponent(typeof(Text))]

public class LiveCOunterUI : MonoBehaviour {

    private Text liveText;
	
	void Awake () {
        liveText = GetComponent<Text>();

	}
	
	// Update is called once per frame
	void Update () {
        liveText.text = "LIVES :" + GameMaster.RemainingLives.ToString();
	}
}
