﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class MenuManager : MonoBehaviour {

    [SerializeField]
    string mouseHoverSound = "HoverSound";
    [SerializeField]
    string buttonPressSound = "ButtonePress";

    AudioManager audioManager;

    private void Start()
    {
        audioManager = AudioManager.instance;
        if (audioManager == null)
            Debug.LogError("No Audio Manager Reference found !");
    }

    public void StartGame()
    {
        audioManager.PlaySound(buttonPressSound);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
	
    public void QuitGame()
    {
        audioManager.PlaySound(buttonPressSound);
        Debug.Log("Quit Application");
        Application.Quit();
    }

    public void OnMouseHover()
    {   
        audioManager.PlaySound(mouseHoverSound);   
    }
  
}
